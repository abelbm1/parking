library ieee;
use ieee.std_logic_1164.all;

entity contador_plazas_tb is
end contador_plazas_tb;

architecture tb of contador_plazas_tb is

    component contador_plazas
        port (PLAZAS      : in std_logic_vector (11 downto 0);
              LEDS        : out std_logic_vector (11 downto 0);
              CLK         : in std_logic;
              FULL        : out std_logic;
              RESET       : in std_logic);
    end component;

    signal PLAZAS         : std_logic_vector (11 downto 0);
    signal LEDS           : std_logic_vector (11 downto 0);
    signal CLK            : std_logic := '0';
    signal RESET          : std_logic;
    signal FULL           : std_logic := '0';

    constant TbPeriod : time := 10 ns; -- EDIT Put right period here
    signal TbSimEnded : std_logic := '0';

begin

    dut : contador_plazas
    port map (PLAZAS         => PLAZAS,
              LEDS           => LEDS,
              CLK            => CLK,
              FULL           => FULL,
              RESET          => RESET);

    -- Clock generation
    CLK <= not CLK after TbPeriod/2 when TbSimEnded /= '1' else '0';
    -- EDIT: Check that clk is really your main clock signal
    stimuli : process
    begin
        PLAZAS <= "000000000000";
        RESET <= '0', '1' after 10 ns;
        wait for 100 ns;
        PLAZAS <= "000000000011";
        wait for 100 ns;
        PLAZAS <= "000000001111";
        wait for 100 ns;
        PLAZAS <= "000000111111";
        wait for 100 ns;
        PLAZAS <= "000011111111";
        wait for 100 ns;
        PLAZAS <= "001111111111";
        wait for 100 ns;
        PLAZAS <= "111111111111";
        wait for 100 ns;
        PLAZAS <= "000000000000";
        wait for 100 ns;
        -- EDIT Add stimuli here

        -- Stop the clock and hence terminate the simulation
        TbSimEnded <= '1';
        wait;
    end process;

end tb;