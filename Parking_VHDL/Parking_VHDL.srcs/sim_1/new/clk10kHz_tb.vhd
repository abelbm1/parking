library ieee;
use ieee.std_logic_1164.all;

entity clk10kHz_tb is
end clk10kHz_tb;

architecture tb of clk10kHz_tb is

    component clk10kHz
        port (CLK       : in std_logic;
              CLK_10khz : out std_logic);
    end component;

    signal CLK       : std_logic;
    signal CLK_10khz : std_logic;

    constant TbPeriod : time := 10 ns; -- EDIT Put right period here
    signal TbClock : std_logic := '0';
    signal TbSimEnded : std_logic := '0';

begin

    dut : clk10kHz
    port map (CLK       => CLK,
              CLK_10khz => CLK_10khz);

    -- Clock generation
    TbClock <= not TbClock after TbPeriod/2 when TbSimEnded /= '1' else '0';

    -- EDIT: Check that CLK is really your main clock signal
    CLK <= TbClock;

    stimuli : process
    begin
        -- EDIT Adapt initialization as needed

        -- Reset generation
        --  EDIT: Replace YOURRESETSIGNAL below by the name of your reset as I haven't guessed it

        -- EDIT Add stimuli here
        wait for 10000* TbPeriod;

        -- Stop the clock and hence terminate the simulation
        TbSimEnded <= '1';
        wait;
    end process;

end tb;