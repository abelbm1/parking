library ieee;
use ieee.std_logic_1164.all;

entity timer_tb is
end timer_tb;

architecture tb of timer_tb is

    component timer
        port (RESET     : in std_logic;
              MODULO : in positive; --cantidad temporizada 
              CLK_IN : in std_logic;
              SALIDA  : out std_logic);
    end component;

    signal RESET     : std_logic;
    signal CLK_IN : std_logic;
    signal SALIDA  : std_logic;
    signal MODULO : positive := 10;

    constant TbPeriod : time := 10 ns; -- EDIT Put right period here
    signal TbClock : std_logic := '0';
    signal TbSimEnded : std_logic := '0';

begin

    dut : timer
    port map (RESET     => RESET,
              CLK_IN => CLK_IN,
              MODULO => MODULO,
              SALIDA  => SALIDA);

    -- Clock generation
    TbClock <= not TbClock after TbPeriod/2 when TbSimEnded /= '1' else '0';

    --  EDIT: Replace YOURCLOCKSIGNAL below by the name of your clock as I haven't guessed it
    --  YOURCLOCKSIGNAL <= TbClock;
    stimuli : process
    begin
        -- EDIT Adapt initialization as needed
        -- Reset generation
        -- EDIT: Check that rst is really your reset signal
        

        -- EDIT Add stimuli here
        wait for 100 * TbPeriod;
        
        -- Stop the clock and hence terminate the simulation
        TbSimEnded <= '1';
        wait;
    end process;
    
    process (TbClock)
    begin
        CLK_IN <= TbClock;
    end process;
    process 
    begin
        RESET <= '0';
        wait for 10 ns;
        RESET <= '1';
        wait for 300 ns;
    end process;
end tb;
