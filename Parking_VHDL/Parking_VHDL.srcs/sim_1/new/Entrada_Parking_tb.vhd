library ieee;
use ieee.std_logic_1164.all;

entity Entrada_Parking_tb is
end Entrada_Parking_tb;

architecture tb of Entrada_Parking_tb is

    component Entrada_Parking
        port (CLK           : in std_logic;
              RESET         : in std_logic;
              BUTTON_ENTRY  : in std_logic;
              BUTTON_EXIT   : in std_logic;
              BUTTON_TICKET_OFF : in std_logic;
              CONT          : out natural;
              TICKET        : out std_logic;
              FULL          : out std_logic);
    end component;

    signal CLK           : std_logic;
    signal RESET         : std_logic;
    signal BUTTON_ENTRY  : std_logic;
    signal BUTTON_EXIT   : std_logic;
    signal BUTTON_TICKET_OFF : std_logic;
    signal CONT          : natural;
    signal TICKET        : std_logic;
    signal FULL          : std_logic;

    constant TbPeriod : time := 10 ns; -- EDIT Put right period here
    signal TbClock : std_logic := '0';
    signal TbSimEnded : std_logic := '0';

begin

    dut : Entrada_Parking
    port map (CLK           => CLK,
              RESET         => RESET,
              BUTTON_ENTRY  => BUTTON_ENTRY,
              BUTTON_EXIT   => BUTTON_EXIT,
              BUTTON_TICKET_OFF => BUTTON_TICKET_OFF,
              CONT          => CONT,
              TICKET        => TICKET,
              FULL          => FULL);

    -- Clock generation
    TbClock <= not TbClock after TbPeriod/2 when TbSimEnded /= '1' else '0';

    -- EDIT: Check that CLK is really your main clock signal
    CLK <= TbClock;

    stimuli : process
    begin
        -- EDIT Adapt initialization as needed
        BUTTON_ENTRY <= '0';
        BUTTON_EXIT  <= '0';
        BUTTON_TICKET_OFF  <= '0';
        -- Reset generation
        -- EDIT: Check that RESET is really your reset signal
        RESET <= '0', '1' after 10 ns;
        wait for 100 ns;
        for i in 1 to 15 loop
        BUTTON_ENTRY<='1';
        BUTTON_TICKET_OFF<='1';
        wait for 10 ns;
        BUTTON_ENTRY <= '0';
        BUTTON_TICKET_OFF<='0';
        wait for 20 ns;
        wait for 30 ns;
        BUTTON_ENTRY<='1';
        BUTTON_TICKET_OFF<='1';
        wait for 10 ns;
        BUTTON_ENTRY <= '0';
        BUTTON_TICKET_OFF<='0';
        wait for 20 ns;
        BUTTON_EXIT <= '1';
        wait for 10 ns;
        BUTTON_EXIT <= '0';
        wait for 50 ns;
        end loop;

        -- EDIT Add stimuli here
        wait for 100 * TbPeriod;

        -- Stop the clock and hence terminate the simulation
        TbSimEnded <= '1';
        wait;
    end process;

end tb;