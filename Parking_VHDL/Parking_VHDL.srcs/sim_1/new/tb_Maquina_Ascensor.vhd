library ieee;
use ieee.std_logic_1164.all;

entity Maquina_Ascensor_tb is
end Maquina_Ascensor_tb;

architecture tb of Maquina_Ascensor_tb is

    component Maquina_Ascensor
        port (RESET       : in std_logic;
              CLK         : in std_logic;
              BUTTON_UP   : in std_logic;
              BUTTON_DOWN : in std_logic;
              SENSOR_UP   : in std_logic;
              SENSOR_IN   : in std_logic;
              SENSOR_DOWN : in std_logic;
              FULL        : in std_logic;
              FULL_EXIT   : out std_logic;
              LIGHT_UP    : out std_logic_vector (2 downto 0);
              LIGHT_DOWN  : out std_logic_vector (2 downto 0));
    end component;

    signal RESET       : std_logic;
    signal CLK         : std_logic;
    signal BUTTON_UP   : std_logic;
    signal BUTTON_DOWN : std_logic;
    signal SENSOR_UP   : std_logic;
    signal SENSOR_IN   : std_logic;
    signal SENSOR_DOWN : std_logic;
    signal FULL        : std_logic;
    signal FULL_EXIT   : std_logic;
    signal LIGHT_UP    : std_logic_vector (2 downto 0);
    signal LIGHT_DOWN  : std_logic_vector (2 downto 0);

    constant TbPeriod : time := 10 ns; -- EDIT Put right period here
    signal TbClock : std_logic := '0';
    signal TbSimEnded : std_logic := '0';

begin

    dut : Maquina_Ascensor
    port map (RESET       => RESET,
              CLK         => CLK,
              BUTTON_UP   => BUTTON_UP,
              BUTTON_DOWN => BUTTON_DOWN,
              SENSOR_UP   => SENSOR_UP,
              SENSOR_IN   => SENSOR_IN,
              SENSOR_DOWN => SENSOR_DOWN,
              FULL        => FULL,
              FULL_EXIT   => FULL_EXIT,
              LIGHT_UP    => LIGHT_UP,
              LIGHT_DOWN  => LIGHT_DOWN);

    -- Clock generation
    TbClock <= not TbClock after TbPeriod/2 when TbSimEnded /= '1' else '0';

    -- EDIT: Check that CLK is really your main clock signal
    CLK <= TbClock;

    stimuli : process
    begin
        -- EDIT Adapt initialization as needed
        BUTTON_UP <= '0';
        BUTTON_DOWN <= '0';
        SENSOR_UP <= '1';
        SENSOR_IN <= '0';
        SENSOR_DOWN <= '0';
        FULL <= '0';

        -- Reset generation
        -- EDIT: Check that RESET is really your reset signal
        RESET <= '0', '1' after 10 ns;
        wait for 100 ns;
        BUTTON_UP<='1';
        wait for 20 ns;
        SENSOR_IN <= '1';
        wait for 5 ns;
        BUTTON_UP <= '0';
        SENSOR_UP <='0'; 
        wait for 100 ns;
        SENSOR_DOWN <= '1';
        SENSOR_IN <= '0';
        wait for 200 ns;
        BUTTON_UP<='1';
        wait for 20 ns;
        BUTTON_UP<='0';
        SENSOR_DOWN <= '0';
        wait for 100 ns;
        SENSOR_UP <= '1';
        BUTTON_UP<='1';
        wait for 20 ns;
        SENSOR_IN <= '1';
        wait for 20 ns;
        BUTTON_UP <= '0';
        SENSOR_UP <='0'; 
        wait for 100 ns;
        SENSOR_DOWN <= '1';
        SENSOR_IN <= '0';
        wait for 200 ns;
        BUTTON_DOWN <= '1';
        SENSOR_IN <= '1';
        wait for 20 ns;
        BUTTON_DOWN <= '0';
        SENSOR_DOWN <= '0';
        wait for 100 ns;
        SENSOR_UP <= '1';
        SENSOR_IN <= '0';
        FULL <= '1';
        wait for 50 ns;
        SENSOR_IN <= '1';
        wait for 50 ns;
        BUTTON_UP<='1';
        wait for 20 ns;
        BUTTON_UP<='0';
        wait for 200 ns;
        -- Stop the clock and hence terminate the simulation
        TbSimEnded <= '1';
        wait;
    end process;

end tb;
