library ieee;
use ieee.std_logic_1164.all;

entity multiplexor_tb is
end multiplexor_tb;

architecture tb of multiplexor_tb is

    component multiplexor
        port (rst     : in std_logic;
              clk     : in std_logic;
              entrada : in positive;
              canal_1 : out positive;
              canal_2 : out std_logic_vector (7 downto 0));
    end component;

    signal rst     : std_logic;
    signal clk     : std_logic := '0';
    signal entrada : natural ;
    signal canal_1 : natural;
    signal canal_2 : std_logic_vector (7 downto 0);
    constant TbPeriod : time := 10 ns; -- EDIT Put right period here
    signal TbSimEnded : std_logic := '0';

begin

    dut : multiplexor
    port map (rst     => rst,
              clk     => clk,
              entrada => entrada,
              canal_1 => canal_1,
              canal_2 => canal_2);

    -- Clock generation
    clk <= not clk after TbPeriod/2 when TbSimEnded /= '1' else '0';

    --  EDIT: Replace YOURCLOCKSIGNAL below by the name of your clock as I haven't guessed it
    --  YOURCLOCKSIGNAL <= TbClock;

    stimuli : process
    begin
        -- EDIT Adapt initialization as needed
        -- Reset generation
        -- EDIT: Check that rst is really your reset signal
        rst <= '0';
        wait for 50 ns;
        rst <= '1';
        wait for 100 ns;

        -- EDIT Add stimuli here
        entrada <= 0;
        wait for 2000 ns;
        entrada <= 12;
        wait for 2000 ns;
        entrada <= 5;
        wait for 2000 ns;
        entrada <= 19;
        wait for 2000 ns;
        entrada <= 12;
        wait for 100 * TbPeriod;

        -- Stop the clock and hence terminate the simulation
        TbSimEnded <= '1';
        wait;
    end process;

end tb;
