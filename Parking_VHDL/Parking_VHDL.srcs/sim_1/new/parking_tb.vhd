library ieee;
use ieee.std_logic_1164.all;

entity parking_tb is
end parking_tb;

architecture tb of parking_tb is

    component parking
        port (BUTTON_UP         : in std_logic;
              BUTTON_DOWN       : in std_logic;
              BUTTON_ENTRY      : in std_logic;
              BUTTON_EXIT       : in std_logic;
              BUTTON_TICKET_OFF : in std_logic;
              SENSOR_UP         : in std_logic;
              SENSOR_IN         : in std_logic;
              SENSOR_DOWN       : in std_logic;
              PLAZAS            : in std_logic_vector (11 downto 0);
              CLK               : in std_logic;
              RESET             : in std_logic;
              DISPLAY_PLAZAS    : out std_logic_vector (6 downto 0);
              LEDS_PLAZAS       : out std_logic_vector (11 downto 0);
              LEDS_UP           : out std_logic_vector (2 downto 0);
              LEDS_DOWN         : out std_logic_vector (2 downto 0);
              LED_FULL          : out std_logic;
              LED_FULL_DOWN     : out std_logic;
              LED_TICKET        : out std_logic;
              digsel            : out std_logic_vector (7 downto 0));
    end component;
    signal BUTTON_UP         : std_logic := '0';
    signal BUTTON_DOWN       : std_logic := '0';
    signal BUTTON_ENTRY      : std_logic := '0';
    signal BUTTON_EXIT       : std_logic := '0';
    signal BUTTON_TICKET_OFF : std_logic := '0';
    signal SENSOR_UP         : std_logic := '0';
    signal SENSOR_IN         : std_logic := '0';
    signal SENSOR_DOWN       : std_logic := '0';
    signal PLAZAS            : std_logic_vector (11 downto 0) := (others => '0');
    signal CLK               : std_logic := '0';
    signal RESET             : std_logic;
    signal DISPLAY_PLAZAS    : std_logic_vector (6 downto 0) := (others => '0');
    signal LEDS_PLAZAS       : std_logic_vector (11 downto 0) := (others => '0');
    signal LEDS_UP           : std_logic_vector (2 downto 0) := (others => '0');
    signal LEDS_DOWN         : std_logic_vector (2 downto 0) := (others => '0');
    signal LED_FULL          : std_logic := '0';
    signal LED_FULL_DOWN     : std_logic := '0';
    signal LED_TICKET        : std_logic := '0';
    signal digsel            : std_logic_vector (7 downto 0) := (others => '0');

    constant TbPeriod : time := 10 ns; -- EDIT Put right period here
    signal TbSimEnded : std_logic := '0';

begin

    dut : parking
    port map (BUTTON_UP         => BUTTON_UP,
              BUTTON_DOWN       => BUTTON_DOWN,
              BUTTON_ENTRY      => BUTTON_ENTRY,
              BUTTON_EXIT       => BUTTON_EXIT,
              BUTTON_TICKET_OFF => BUTTON_TICKET_OFF,
              SENSOR_UP         => SENSOR_UP,
              SENSOR_IN         => SENSOR_IN,
              SENSOR_DOWN       => SENSOR_DOWN,
              PLAZAS            => PLAZAS,
              CLK               => CLK,
              RESET             => RESET,
              DISPLAY_PLAZAS    => DISPLAY_PLAZAS,
              LEDS_PLAZAS       => LEDS_PLAZAS,
              LEDS_UP           => LEDS_UP,
              LEDS_DOWN         => LEDS_DOWN,
              LED_FULL          => LED_FULL,
              LED_FULL_DOWN     => LED_FULL_DOWN,
              LED_TICKET        => LED_TICKET,
              digsel            => digsel
              );

    -- Clock generation
    CLK <= not CLK after TbPeriod/2 when TbSimEnded /= '1' else '0';

    -- EDIT: Check that BUTTON_TICKET_OFF is really your main clock signal

    stimuli : process
    begin
        -- EDIT Adapt initialization as needed

        -- Reset generation
        -- EDIT: Check that RESET is really your reset signal
        RESET <= '0';
        SENSOR_UP <= '1';
        wait for 100 ns;
        RESET <= '1';
        wait for 100 ns;

        -- EDIT Add stimuli here
        --for i in 0 to 5 loop
            BUTTON_ENTRY <= '1';
            wait for 50 ns;
            BUTTON_ENTRY <= '0';
            wait for 100 ns;
            BUTTON_TICKET_OFF <= '1';
            wait for 50 ns;
            BUTTON_TICKET_OFF <= '0';
            wait for 200 ns;
            SENSOR_UP <= '1';
            SENSOR_IN <= '1';
            wait for 100 ns;
            BUTTON_UP <= '1';
            wait for 20 ns;
            BUTTON_UP <= '0';
            SENSOR_UP <= '0';
            wait for 200 ns;
            RESET <= '0';
            wait for 20 ns;
            RESET <= '1';
            wait for 500 ns;
            SENSOR_DOWN <= '1';
            wait for 50 ns;
            SENSOR_IN <= '0';
            wait for 100 ns;
            RESET <= '0';
            wait for 20 ns;
            RESET <= '1';
            wait for 200 ns;
            BUTTON_DOWN <= '1';
            SENSOR_IN <= '1';
            wait for 20 ns;
            BUTTON_DOWN <= '0';
            SENSOR_DOWN <= '0';
            wait for 200 ns;
            SENSOR_UP <= '1';
            wait for 50 ns;
            SENSOR_IN <= '0';
            wait for 500 ns;
           -- SENSOR_DOWN <= '0';
          --  wait for 100 ns;
        --end loop;
        wait for 100 * TbPeriod;

        -- Stop the clock and hence terminate the simulation
        TbSimEnded <= '1';
        wait;
    end process;

end tb;