library ieee;
use ieee.STD_LOGIC_1164.ALL;
USE ieee.std_logic_arith.ALL;
USE ieee.std_logic_unsigned.ALL;

entity Maquina_Ascensor is
    Port ( RESET                    : in STD_LOGIC;
           CLK                      : in STD_LOGIC;
           BUTTON_UP                : in STD_LOGIC;
           BUTTON_DOWN              : in STD_LOGIC;
           SENSOR_UP                : in std_logic;
           SENSOR_IN                : in std_logic;
           SENSOR_DOWN              : in std_logic;
           FULL                     : in STD_LOGIC;
           FULL_EXIT                : out std_logic;
           LIGHT_UP                 : out STD_LOGIC_VECTOR(2 downto 0);
           LIGHT_DOWN               : out STD_LOGIC_VECTOR(2 downto 0)
     );
end Maquina_Ascensor;

architecture behavioral of Maquina_Ascensor is
  type STATES is (S0, S1, S2);
  -- S0: ascensor espera
  -- S1: ascensor baja 
  -- S2: ascensor sube 
    signal current_state: STATES := S0;
    signal next_state: STATES;
begin
    state_register: process (RESET, CLK)
 begin
   if (RESET = '0') then
      current_state <= next_state;
   elsif (CLK'event and CLK='1') then
      current_state <= next_state;
   end if;
 end process;
 
 nextstate_decod: process (BUTTON_UP,BUTTON_DOWN,current_state,SENSOR_DOWN,SENSOR_IN,SENSOR_UP)
 begin
    next_state <= current_state;
    case current_state is
        when S0 =>
            if ((BUTTON_UP = '1' and SENSOR_UP = '1' and FULL = '0' and SENSOR_IN = '1') or 
            (BUTTON_DOWN = '1' and SENSOR_UP = '1' and SENSOR_IN = '0')) then
            -- Si coche en calle quiere bajar, est� dentro del ascensor, el ascensor est� arriba y hay plazas libres
            -- O si coche en parking quiere salir, pero ascensor est� arriba y vac�o
                next_state <= S1; --Baja    
            elsif ((BUTTON_UP = '1' and SENSOR_IN = '0' and SENSOR_DOWN = '1' and FULL = '0') or 
            (BUTTON_DOWN = '1' and SENSOR_DOWN = '1' and SENSOR_IN = '1')) then 
            -- Si coche quiere entrar, ascensor est� abajo, parking libre
            -- O coche quiere salir, est� dentro del ascensor y el ascensor est� abajo
                next_state <= S2; --Sube
            end if;
        when S1 =>
            -- ascensor baja hasta llegar al parking
            if SENSOR_DOWN = '1' then 
                next_state <= S0; --reposo
            end if;
        when S2 => 
            -- ascensor sube hasta llegar a la calle
            if SENSOR_UP = '1' then 
                next_state <= S0; --reposo
            end if;
        when others =>
            next_state <= S0;
    end case;

    
 end process;

 output_decod: process (current_state, FULL)
 begin
 LIGHT_UP <= "010";
 LIGHT_DOWN <= "100";
 FULL_EXIT <= '0';
 if (FULL = '1') then
        FULL_EXIT <= '1';
    else
        FULL_EXIT <= '0';
 end if;
     case current_state is
        when S0 => -- PARADO
            if(SENSOR_UP= '1' or (RESET ='0' and SENSOR_DOWN ='0')) then
                LIGHT_UP <= "010";
                LIGHT_DOWN <= "100";
            elsif(SENSOR_DOWN = '1' or (RESET ='0' and SENSOR_UP ='0' and SENSOR_IN = '0')) then
                LIGHT_UP <= "100";
                LIGHT_DOWN <= "010";
            end if;
        when S1 => -- BAJANDO
            LIGHT_UP <= "110";
            LIGHT_DOWN <= "110";
        when S2 => -- SUBIENDO
            LIGHT_UP <= "110";
            LIGHT_DOWN <= "110";
     end case;
 end process; 
end behavioral;