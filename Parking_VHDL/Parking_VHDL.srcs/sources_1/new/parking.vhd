library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity parking is
Generic ( TOTAL : NATURAL := 12 );
    Port ( BUTTON_UP             : in STD_LOGIC;--boton de arriba del ascensor
           BUTTON_DOWN           : in STD_LOGIC;--boton de abajo del ascensor
           BUTTON_ENTRY          : in std_logic; --boton de entrada
           BUTTON_EXIT           : in std_logic; --boton de salida
           BUTTON_TICKET_OFF     : in std_logic; --boton de apagar ticket
           SENSOR_UP             : in std_logic; --sensor que marca que el ascensor est� arriba (es un switch)
           SENSOR_IN             : in std_logic;--sensor que marca que hay un coche dentro del acensor (es un switch)
           SENSOR_DOWN           : in std_logic;--sensor que marca que el ascensor est� abajo (es un switch)
           PLAZAS                : in STD_LOGIC_VECTOR (TOTAL-1 downto 0); -- sensor que indica todas las plazas del parking (son switches)
           CLK                   : in STD_LOGIC;--se�al de reloj
           RESET                 : in STD_LOGIC;--se�al de reset
           DISPLAY_PLAZAS        : out STD_LOGIC_VECTOR (6 downto 0);--display 7 segmentos numero de plazas
           LEDS_PLAZAS           : out STD_LOGIC_VECTOR (TOTAL-1 downto 0); --indicadores luminosos de las plazas del parking
           LEDS_UP               : out STD_LOGIC_vector(2 downto 0);--semaforo de la calle
           LEDS_DOWN             : out STD_LOGIC_vector(2 downto 0);--semaforo del parking
           LED_FULL              : out STD_LOGIC;--indicador global parking libre/lleno
           LED_FULL_DOWN         : out STD_LOGIC;--indicador plazas del parking libre/lleno
           LED_TICKET            : out std_logic; -- indica la emisi�n de un ticket    
           digsel                : out std_logic_vector(7 downto 0)--digitos del display de 7 segmentos
    );
end parking;

architecture Behavioral of parking is
--declaracion del sincronizador
 component SYNCHRNZR
    port (
        CLK      : in  std_logic; 
        ASYNC_IN : in  std_logic; 
        SYNC_OUT : out std_logic
    );
    end component;
--declaracion del detector de flancos
    component EDGEDTCTR 
    port (
        CLK      : in  std_logic; 
        SYNC_IN  : in  std_logic; 
        EDGE     : out std_logic
    );
    end component;
    --declaracion de la entrada y salida del parking
    component Entrada_parking
    port (
        CLK : in std_logic;
        RESET : in std_logic;
        BUTTON_ENTRY : in std_logic;
        BUTTON_EXIT : in std_logic;
        BUTTON_TICKET_OFF : in std_logic;
        CONT : out natural;
        TICKET : out std_logic;
        FULL : out std_logic
    );
    end component;
--declaracion de la maquina de estados del ascensor
    component Maquina_Ascensor
    Port ( 
        RESET                    : in STD_LOGIC;
        CLK                      : in STD_LOGIC;
        BUTTON_UP                : in STD_LOGIC;
        BUTTON_DOWN              : in STD_LOGIC;
        SENSOR_UP                : in std_logic;
        SENSOR_IN                : in std_logic;
        SENSOR_DOWN              : in std_logic;
        FULL                     : in STD_LOGIC;
        FULL_EXIT                : out STD_LOGIC;
        LIGHT_UP                 : out STD_LOGIC_VECTOR(2 downto 0);
        LIGHT_DOWN               : out STD_LOGIC_VECTOR(2 downto 0)
     );
    end component;

--declaracion del contador de plazas
    component contador_plazas
    generic (
        WIDTH : natural := 12 -- capacidad m�xima del parking
    );
    Port ( 
           PLAZAS : in STD_LOGIC_VECTOR (WIDTH -1 downto 0);
           LEDS   : out std_logic_vector (WIDTH -1 downto 0);
           CLK    : in std_logic;
           RESET  : in std_logic;
           FULL   : out std_logic
           );
    end component;
    
    component multiplexor is
    Port ( 
        rst : in std_logic;
        clk : in std_logic;
        entrada : in natural;
        canal_1 : out natural;
        canal_2 : out std_logic_vector (7 downto 0)
    );
    end component;

    component decoder
    PORT ( 
        code : natural := 0; 
        led  : OUT std_logic_vector(6 DOWNTO 0) 
    ); 
    end component;
    
--se�ales intermedias
signal BUTTON_UP_SYNC_EDGE : std_logic;
signal BUTTON_UP_EDGE_ASC : std_logic;
signal BUTTON_DOWN_SYNC_EDGE : std_logic;
signal BUTTON_DOWN_EDGE_ASC : std_logic;
signal BUTTON_ENTRY_SYNC_EDGE : std_logic;
signal BUTTON_ENTRY_EDGE_ENT : std_logic;
signal BUTTON_EXIT_SYNC_EDGE : std_logic;
signal BUTTON_EXIT_EDGE_ENT : std_logic;
signal BUTTON_TICKETOFF_SYNC_EDGE : std_logic;
signal BUTTON_TICKETOFF_ENT : std_logic;
signal FULL_DOWN_AUX : std_logic;
signal CONT_AUX : natural;
signal CANAL1 : natural;

begin
--instanciaciones para el boton de arriba del ascensor
    UP_sync : SYNCHRNZR
    port map(
        CLK => CLK, 
        ASYNC_IN => BUTTON_UP, 
        SYNC_OUT => BUTTON_UP_SYNC_EDGE
    );
    UP_edge : EDGEDTCTR
    port map(
        CLK => CLK,
        SYNC_IN => BUTTON_UP_SYNC_EDGE,
        EDGE => BUTTON_UP_EDGE_ASC
    );
    --instanciaciones para el boton de abajo del ascensor
    DOWN_sync : SYNCHRNZR
    port map(
        CLK => CLK, 
        ASYNC_IN => BUTTON_DOWN, 
        SYNC_OUT => BUTTON_DOWN_SYNC_EDGE
    );
    DOWN_edge : EDGEDTCTR
    port map(
        CLK => CLK,
        SYNC_IN => BUTTON_DOWN_SYNC_EDGE,
        EDGE => BUTTON_DOWN_EDGE_ASC
    );
    --instanciaciones para el boton de entrada
    ENTRY_sync : SYNCHRNZR
    port map(
        CLK => CLK, 
        ASYNC_IN => BUTTON_ENTRY, 
        SYNC_OUT => BUTTON_ENTRY_SYNC_EDGE
    );
    ENTRY_edge : EDGEDTCTR
    port map(
        CLK => CLK,
        SYNC_IN => BUTTON_ENTRY_SYNC_EDGE,
        EDGE => BUTTON_ENTRY_EDGE_ENT
    );
    --instanciaciones para el boton de salida
    EXIT_sync : SYNCHRNZR
    port map(
        CLK => CLK, 
        ASYNC_IN => BUTTON_EXIT, 
        SYNC_OUT => BUTTON_EXIT_SYNC_EDGE
    );
    EXIT_edge : EDGEDTCTR
    port map(
        CLK => CLK,
        SYNC_IN => BUTTON_EXIT_SYNC_EDGE,
        EDGE => BUTTON_EXIT_EDGE_ENT
    );
    --instanciaciones para el boton de recoger ticket
    TICKETOFF_sync : SYNCHRNZR
    port map(
        CLK => CLK, 
        ASYNC_IN => BUTTON_TICKET_OFF, 
        SYNC_OUT => BUTTON_TICKETOFF_SYNC_EDGE
    );
    TICKETOFF_edge : EDGEDTCTR
    port map(
        CLK => CLK,
        SYNC_IN => BUTTON_TICKETOFF_SYNC_EDGE,
        EDGE => BUTTON_TICKETOFF_ENT
    );
     --entrada al parking
    ENT_PARKING: Entrada_Parking
    port map(
        CLK => CLK,
        RESET => RESET,
        BUTTON_ENTRY => BUTTON_ENTRY_EDGE_ENT,
        BUTTON_EXIT =>BUTTON_EXIT_EDGE_ENT,
        BUTTON_TICKET_OFF => BUTTON_TICKETOFF_ENT, 
        CONT => CONT_AUX,
        TICKET => LED_TICKET,
        FULL => LED_FULL
    );
    -- MUX del contador de plazas totales
    gestor_display : multiplexor
    port map(
        rst => RESET,
        clk => CLK,
        entrada => CONT_AUX,
        canal_1 => CANAL1,
        canal_2 => digsel
    );
    -- decodificador del contador de plazas totales
   decoder_display : decoder
   port map (
        code => CANAL1,
        led  => DISPLAY_PLAZAS
   );
    --ascensor
    ME_ascensor : Maquina_Ascensor
    port map(
        RESET => RESET,
        CLK => CLK,
        BUTTON_UP => BUTTON_UP_EDGE_ASC,
        BUTTON_DOWN => BUTTON_DOWN_EDGE_ASC,
       -- BUTTON_UP => BUTTON_UP,
        --BUTTON_DOWN => BUTTON_DOWN,
        SENSOR_UP   => SENSOR_UP,
        SENSOR_IN   => SENSOR_IN,
        SENSOR_DOWN => SENSOR_DOWN,
        FULL => FULL_DOWN_AUX,
        FULL_EXIT => LED_FULL_DOWN,
        LIGHT_UP => LEDS_UP,
        LIGHT_DOWN => LEDS_DOWN
    );
    --contador de plazas
    contador_plazas_ocupadas_down : contador_plazas
    port map (
           RESET => RESET,
           CLK => CLK,
           PLAZAS => PLAZAS,
           LEDS => LEDS_PLAZAS,
           FULL => FULL_DOWN_AUX
    );
end Behavioral;
