library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity gestor_entrada is
    Port ( 
        CLK : in std_logic;
        RESET : in std_logic;
        BOTON_ENTRADA : in std_logic;
        BOTON_SALIDA : in std_logic;
        CONT : out natural;
        TICKET : out std_logic;
        FULL : out std_logic
    );
end gestor_entrada;

architecture Behavioral of gestor_entrada is
signal ticket_i : std_logic := '0';
signal cont_i : natural := 0;
signal FULL_i : std_logic := '0';
begin

    gestion_botones : process(CONT)
    begin
        if CONT < 20 then
            if BOTON_ENTRADA = '1' then
                cont_i <= cont_i + 1;
                ticket_i <= '1';
            end if;
        end if;
        if BOTON_SALIDA = '1' then
            cont_i <= cont_i - 1;
        end if;
        TICKET <= ticket_i;
        CONT <= cont_i;
    end process;
    
    gestion_capacidad : process (CONT)
    begin
        if CONT = 20 then
            FULL_i <= '1';
        else
            FULL_i <= '0';
        end if;
    FULL <= FULL_i;
    end process;
    
end Behavioral;
