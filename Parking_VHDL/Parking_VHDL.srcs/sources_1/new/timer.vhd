library IEEE;
use IEEE.STD_LOGIC_1164.ALL;


entity timer is
    Port ( 
        RESET : in std_logic; --rst negado
        CLK_IN : in std_logic; --se�al clk preescalada
        MODULO : in positive; --cantidad temporizada 
        SALIDA : out std_logic --salida que ser� 1 si termina el contador
    );
end timer;

architecture Behavioral of timer is
signal cuenta : natural :=100;  
signal salida_i : std_logic; 
signal CLK_1Hz: std_logic := '0';

component clk1Hz
Port (
    CLK : in  STD_LOGIC;
    CLK_1Hz: out STD_LOGIC
);  
end component;
    begin
    Inst_preescaler: clk1Hz
    port map(
        CLK => CLK_IN,
        CLK_1Hz => CLK_1Hz
    );
        process (RESET, CLK_1Hz)
        
        begin
            if RESET = '0' then
                cuenta <= 0;
            elsif rising_edge(CLK_1Hz) then
                cuenta <= (cuenta + 1);
            end if;
            if cuenta = modulo - 1 then
                salida_i <= '1';
            else
                salida_i <= '0';
            end if; 
        salida <= salida_i;
        end process;
end Behavioral;
