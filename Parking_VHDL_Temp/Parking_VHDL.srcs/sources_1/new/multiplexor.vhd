library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
USE ieee.std_logic_arith.ALL;
USE ieee.std_logic_unsigned.ALL;


entity multiplexor is
    Port ( 
    rst : in std_logic;
    clk : in std_logic;
    entrada : in NATURAL;
    canal_1 : out NATURAL;--numero
    canal_2 : out std_logic_vector (7 downto 0)--selector
    );
end multiplexor;

architecture Behavioral of multiplexor is
    signal decenas : NATURAL := 0;
    signal unidades : NATURAL := 0;
    signal CLK_10kHz: std_logic := '0';
    signal canal1 : natural := 0;
    signal canal2 : std_logic_vector (7 downto 0) := "11111111";
    
    component clk10kHz
    Port (
       CLK : in  STD_LOGIC;
       CLK_10khz: out STD_LOGIC
    );  
    end component;
begin
    Inst_preescaler: clk10kHz --clk10kHz
    port map(
        CLK => CLK,
        CLK_10khz => CLK_10kHz
    );
    process (entrada)
    begin
        if entrada < 10 then
            unidades <= entrada;
            decenas <= 0;
        elsif entrada < 20 then
            decenas <= 1;
            unidades <= entrada mod 10;
        end if;
    end process;
    
    process (rst, CLK_10kHz)
    begin
        if rst = '0' then
            canal1 <= 0;
            canal2 <= (others => '1');
        elsif CLK_10kHz = '1' then
            canal1 <= decenas;
            canal2 <= "11111101";
        elsif CLK_10kHz = '0' then
            canal1 <= unidades;
            canal2 <= "11111110";
        end if;
        canal_1 <= canal1;
        canal_2 <= canal2;
    end process;
end Behavioral;

