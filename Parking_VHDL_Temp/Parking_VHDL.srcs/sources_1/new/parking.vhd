library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity parking is
    Port ( BUTTON_UP             : in STD_LOGIC;--boton de arriba del ascensor
           BUTTON_DOWN           : in STD_LOGIC;--boton de abajo del ascensor
           BUTTON_ENTRY          : in std_logic; --boton de entrada
           BUTTON_EXIT           : in std_logic; --boton de salida
           BUTTON_TICKET_OFF     : in std_logic; --boton de apagar ticket
           PLAZAS                : in STD_LOGIC_VECTOR (6 downto 0);
           CLK                   : in STD_LOGIC;--se�al de reloj
           RESET                 : in STD_LOGIC;--se�al de reset
           LEDS_UP               : out STD_LOGIC_vector(2 downto 0);--semaforo de la calle
           LEDS_DOWN             : out STD_LOGIC_vector(2 downto 0);--semaforo del parking
           DISPLAY_PLAZAS        : out STD_LOGIC_VECTOR (6 downto 0);--display 7 segmentos numero de plazas
           FULL                  : out STD_LOGIC;--indicador parking libre/lleno
           FULL_DOWN             : out STD_LOGIC;--indicador parking libre/lleno
           INDICADOR_PLAZAS      : out STD_LOGIC_VECTOR (6 downto 0);--indicadores plazas libres/ocupadas
           LED_TICKET            : out std_logic; -- indica la emisi�n de un ticket
           LEDS_ESTADOS          : out std_logic_vector (2 downto 0); -- indica el estado actual del ascensor
           digsel                : out std_logic_vector(7 downto 0)--digitos del display de 7 segmentos
    );
end parking;

architecture Behavioral of parking is
--declaracion del sincronizador
 component SYNCHRNZR
    port (
        CLK      : in  std_logic; 
        ASYNC_IN : in  std_logic; 
        SYNC_OUT : out std_logic
    );
    end component;
--declaracion del detector de flancos
    component EDGEDTCTR 
    port (
        CLK      : in  std_logic; 
        SYNC_IN  : in  std_logic; 
        EDGE     : out std_logic
    );
    end component;
    --declaracion de la entrada y salida del parking
    component Entrada_parking
    port (
        CLK : in std_logic;
        RESET : in std_logic;
        BUTTON_ENTRY : in std_logic;
        BUTTON_EXIT : in std_logic;
        TEMP : in std_logic;
        CONT : out natural;
        TICKET : out std_logic;
        FULL : out std_logic
    );
    end component;
--declaracion de la maquina de estados del ascensor
    component Maquina_Ascensor
    Port ( 
        RESET                    : in STD_LOGIC;
        CLK                      : in STD_LOGIC;
        BUTTON_UP                : in STD_LOGIC;
        BUTTON_DOWN              : in STD_LOGIC;
        FULL                     : in STD_LOGIC;
        TEMP_ASC_UP              : in STD_LOGIC;
        TEMP_ASC_DOWN            : in STD_LOGIC;
        FULL_EXIT                : out STD_LOGIC;
        FLAG_TEMP_ASC_UP         : out std_logic;
        FLAG_TEMP_ASC_DOWN       : out std_logic;
        LIGHT_UP                 : out STD_LOGIC_VECTOR(2 downto 0);
        LIGHT_DOWN               : out STD_LOGIC_VECTOR(2 downto 0);
        LED_BUTTON_DOWN          : out std_logic;
        LED_BUTTON_UP            : out std_logic;
        LIGHT_ESTADOS            : out STD_LOGIC_VECTOR(2 downto 0) -- S0,S1,bus 
     );
    end component;
--declaracion del temporizador
--component preescaler
   --Port (
     --   preescale : in positive;
     --   CLK_IN : in  STD_LOGIC;
      --  RESET  : in  STD_LOGIC;
     --   CLK_OUT: out STD_LOGIC
   -- );
--end component;

component timer_ticket
    Port ( 
        RESET : in std_logic; --rst negado
        CLK_IN : in std_logic; --se�al clk preescalada
        MODULO : in positive; --preescaler en segundos
        SALIDA : out std_logic --salida que ser� 1 si termina el contador
    );
end component;

component timer
    Port ( 
        RESET : in std_logic; --rst negado
        CLK_IN : in std_logic; --se�al clk preescalada
        MODULO : in positive; --preescaler en segundos
        SALIDA : out std_logic --salida que ser� 1 si termina el contador
    );
end component;
    
--declaracion del contador de plazas
    component contador_plazas
    generic (
    WIDTH : positive := 7
    );
    Port ( PLAZAS : in STD_LOGIC_VECTOR (width -1 downto 0);
           LEDS : out STD_LOGIC_VECTOR (width -1 downto 0);
           CLK : in std_logic;
           RESET : in std_logic;
           FULL : out std_logic
           );
    end component;
    
    component multiplexor is
    Port ( 
        rst : in std_logic;
        clk : in std_logic;
        entrada : in natural;
        canal_1 : out natural;
        canal_2 : out std_logic_vector (7 downto 0)
    );
    end component;

    component decoder
    PORT ( 
        code : natural := 0; 
        led  : OUT std_logic_vector(6 DOWNTO 0) 
    ); 
    end component;
    
--se�ales intermedias
signal BUTTON_UP_SYNC_EDGE : std_logic;
signal BUTTON_UP_EDGE_ASC : std_logic;
signal BUTTON_DOWN_SYNC_EDGE : std_logic;
signal BUTTON_DOWN_EDGE_ASC : std_logic;
signal BUTTON_ENTRY_SYNC_EDGE : std_logic;
signal BUTTON_ENTRY_EDGE_ENT : std_logic;
signal BUTTON_EXIT_SYNC_EDGE : std_logic;
signal BUTTON_EXIT_EDGE_ENT : std_logic;
signal FULL_DOWN_AUX : std_logic;
signal CONT_AUX : natural;
signal CANAL1 : natural;
signal CANAL2 : std_logic_vector(7 downto 0); --no se usa
signal TEMP_TICKET_OUT : std_logic;
signal TEMP_ASC_OUT_UP : std_logic;
signal TEMP_ASC_OUT_DOWN : std_logic;
signal TEMP_INIT_ASC_UP: std_logic;
signal TEMP_INIT_ASC_DOWN: std_logic;

begin
--instanciaciones para el boton de arriba del ascensor
    UP_sync : SYNCHRNZR
    port map(
        CLK => CLK, 
        ASYNC_IN => BUTTON_UP, 
        SYNC_OUT => BUTTON_UP_SYNC_EDGE
    );
    UP_edge : EDGEDTCTR
    port map(
        CLK => CLK,
        SYNC_IN => BUTTON_UP_SYNC_EDGE,
        EDGE => BUTTON_UP_EDGE_ASC
    );
    --instanciaciones para el boton de abajo del ascensor
    DOWN_sync : SYNCHRNZR
    port map(
        CLK => CLK, 
        ASYNC_IN => BUTTON_DOWN, 
        SYNC_OUT => BUTTON_DOWN_SYNC_EDGE
    );
    DOWN_edge : EDGEDTCTR
    port map(
        CLK => CLK,
        SYNC_IN => BUTTON_DOWN_SYNC_EDGE,
        EDGE => BUTTON_DOWN_EDGE_ASC
    );
    --instanciaciones para el boton de entrada
    ENTRY_sync : SYNCHRNZR
    port map(
        CLK => CLK, 
        ASYNC_IN => BUTTON_ENTRY, 
        SYNC_OUT => BUTTON_ENTRY_SYNC_EDGE
    );
    ENTRY_edge : EDGEDTCTR
    port map(
        CLK => CLK,
        SYNC_IN => BUTTON_ENTRY_SYNC_EDGE,
        EDGE => BUTTON_ENTRY_EDGE_ENT
    );
    --instanciaciones para el boton de salida
    EXIT_sync : SYNCHRNZR
    port map(
        CLK => CLK, 
        ASYNC_IN => BUTTON_EXIT, 
        SYNC_OUT => BUTTON_EXIT_SYNC_EDGE
    );
    EXIT_edge : EDGEDTCTR
    port map(
        CLK => CLK,
        SYNC_IN => BUTTON_EXIT_SYNC_EDGE,
        EDGE => BUTTON_EXIT_EDGE_ENT
    );
    --contador de plazas
    contador_plazas_ocupadas_down : contador_plazas
    port map (
           PLAZAS => PLAZAS,
           LEDS => INDICADOR_PLAZAS,
           CLK => CLK,
           RESET => RESET,
           FULL => FULL_DOWN_AUX
    );
    gestor_display : multiplexor
    port map(
        rst => RESET,
        clk => CLK,
        entrada => CONT_AUX,
        canal_1 => CANAL1,
        canal_2 => digsel
    );
    --entrada al parking
    ENT_PARKING: Entrada_Parking
    port map(
        CLK => CLK,
        RESET => RESET,
        BUTTON_ENTRY => BUTTON_ENTRY_EDGE_ENT,
        BUTTON_EXIT =>BUTTON_EXIT_EDGE_ENT,
        TEMP =>  BUTTON_TICKET_OFF, --TEMP_TICKET_OUT,
        CONT => CONT_AUX,
        TICKET => LED_TICKET,
        FULL => FULL
    );
    --ascensor
    ME_ascensor : Maquina_Ascensor
    port map(
        RESET => RESET,
        CLK => CLK,
        BUTTON_UP => BUTTON_UP,
        BUTTON_DOWN => BUTTON_DOWN,
        TEMP_ASC_UP => TEMP_ASC_OUT_UP,
        TEMP_ASC_DOWN => TEMP_ASC_OUT_DOWN,
        FLAG_TEMP_ASC_UP => TEMP_INIT_ASC_UP,
        FLAG_TEMP_ASC_DOWN => TEMP_INIT_ASC_DOWN,
        FULL => FULL_DOWN_AUX,
        FULL_EXIT => FULL_DOWN,
        LIGHT_UP => LEDS_UP,
        LIGHT_DOWN => LEDS_DOWN,
        LIGHT_ESTADOS => LEDS_ESTADOS
    );
    
    temporizador_ascensor_up : timer
    port map (
        RESET => TEMP_INIT_ASC_UP,--se�al para iniciar
        CLK_IN => CLK,
        MODULO => 6,
        SALIDA => TEMP_ASC_OUT_UP
    );
    temporizador_ascensor_down : timer
    port map (
        RESET => TEMP_INIT_ASC_DOWN,--se�al para iniciar
        CLK_IN => CLK,
        MODULO => 6,
        SALIDA => TEMP_ASC_OUT_DOWN
    );
   decoder_display : decoder
   port map (
        code => CANAL1,
        led  => DISPLAY_PLAZAS
   );
end Behavioral;
