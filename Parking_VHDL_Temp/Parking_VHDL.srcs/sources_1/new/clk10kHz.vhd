library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
 
entity clk10kHz is
    Port (
        CLK: in  STD_LOGIC;
        CLK_10khz : out STD_LOGIC
    );
end clk10kHz;
 
architecture Behavioral of clk10kHz is
    signal temporal: STD_LOGIC:= '0';
    --signal contador: integer range 0 to 4999 := 0;
    signal contador: integer range 0 to 5 := 0;
begin
    divisor_frecuencia: process (CLK) begin        
        if rising_edge(CLK) then
            --if (contador = 4999) then
            if (contador =  5) then
                temporal <= NOT(temporal);
                contador <= 0;
            else
                contador <= contador+1;
            end if;
        end if;
    end process;
     
    CLK_10khz <= temporal;
    
end Behavioral;