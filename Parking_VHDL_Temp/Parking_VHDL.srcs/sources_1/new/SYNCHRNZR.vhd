library IEEE; 
use IEEE.STD_LOGIC_1164.ALL; 
 
entity SYNCHRNZR is 
  port (  
    CLK      : in  std_logic; 
    ASYNC_IN : in  std_logic; 
    SYNC_OUT : out std_logic 
  ); 
end SYNCHRNZR; 
 
architecture BEHAVIORAL of SYNCHRNZR is 
  signal sreg : std_logic_vector(1 downto 0); 
  signal CLK_10kHz: std_logic := '0';
  
  component clk10kHz
    Port (
       CLK : in  STD_LOGIC;
       CLK_10khz: out STD_LOGIC
    );  
    end component;
    
begin 
Inst_preescaler: clk10kHz --clk10kHz
    port map(
        CLK => CLK,
        CLK_10khz => CLK_10kHz
    );
  --process (CLK)  
  process (CLK) 
  begin 
     if rising_edge(CLK) then   
     --if rising_edge(CLK) then   
       sync_out <= sreg(1); 
       sreg <= sreg(0) & async_in; 
     end if; 
  end process; 
end BEHAVIORAL;
