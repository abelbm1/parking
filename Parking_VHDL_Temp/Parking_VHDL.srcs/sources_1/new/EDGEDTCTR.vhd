library IEEE; 
use IEEE.STD_LOGIC_1164.ALL; 
 
entity EDGEDTCTR is 
  port (
    CLK      : in  std_logic; 
    SYNC_IN  : in  std_logic; 
    EDGE     : out std_logic
  ); 
end EDGEDTCTR; 
 
architecture BEHAVIORAL of EDGEDTCTR is 
  signal sreg : std_logic_vector(2 downto 0); 
  signal CLK_10kHz: std_logic := '0';
  
  component clk10kHz
    Port (
       CLK : in  STD_LOGIC;
       CLK_10khz: out STD_LOGIC
    );  
    end component;
 
begin 

Inst_preescaler: clk10kHz --clk10kHz
    port map(
        CLK => CLK,
        CLK_10khz => CLK_10kHz
    );
    
  process (CLK)
  --process (CLK)   
  begin 
    if rising_edge(CLK) then 
    --if rising_edge(CLK) then 
      sreg <= sreg(1 downto 0) & SYNC_IN; 
    end if;
  end process; 
  
  with sreg select 
    EDGE <= '1' when "100", 
            '0' when others; 
end BEHAVIORAL;