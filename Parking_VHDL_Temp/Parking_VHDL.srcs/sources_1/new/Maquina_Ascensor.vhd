library ieee;
use ieee.STD_LOGIC_1164.ALL;
USE ieee.std_logic_arith.ALL;
USE ieee.std_logic_unsigned.ALL;

entity Maquina_Ascensor is
    Port ( RESET                    : in STD_LOGIC;
           CLK                      : in STD_LOGIC;
           BUTTON_UP                : in STD_LOGIC;
           BUTTON_DOWN              : in STD_LOGIC;
           FULL                     : in STD_LOGIC;
           TEMP_ASC_UP              : in STD_LOGIC;
           TEMP_ASC_DOWN            : in STD_LOGIC;
           FULL_EXIT                : out std_logic;
           FLAG_TEMP_ASC_UP         : out std_logic;
           FLAG_TEMP_ASC_DOWN       : out std_logic;
           LIGHT_UP                 : out STD_LOGIC_VECTOR(2 downto 0);
           LIGHT_DOWN               : out STD_LOGIC_VECTOR(2 downto 0);
           LED_BUTTON_DOWN          : out std_logic; -- PRUEBA BOTONES UP Y DOWN
           LED_BUTTON_UP            : out std_logic;
           LIGHT_ESTADOS            : out STD_LOGIC_VECTOR(2 downto 0) -- S0,S1,S2
     );
end Maquina_Ascensor;

architecture behavioral of Maquina_Ascensor is
  type STATES is (S0, S1, S2);
  -- S0: ascensor espera
  -- S1: ascensor baja 
  -- S2: ascensor sube 
    signal current_state: STATES := S0;
    signal next_state: STATES;
    signal SENSOR_UP : STD_LOGIC := '1';
    signal SENSOR_IN : STD_LOGIC := '0';
    signal SENSOR_DOWN : STD_LOGIC := '0';
begin
    state_register: process (RESET, CLK)
 begin
   if (RESET = '0') then
      current_state <= S0;
   elsif (CLK'event and CLK='1') then
      current_state <= next_state;
   end if;
 end process;
 
 nextstate_decod: process (CLK,BUTTON_UP,BUTTON_DOWN,TEMP_ASC_UP,TEMP_ASC_DOWN,FULL,current_state)
 begin
    next_state <= current_state;
    FLAG_TEMP_ASC_UP <= '1';
    FLAG_TEMP_ASC_DOWN <= '1';
    
    if (TEMP_ASC_UP='1' or (RESET ='0' and SENSOR_DOWN ='0')) then
      SENSOR_UP <= '1';
      SENSOR_DOWN <= '0';
      SENSOR_IN <= '0';
    end if;
    if (TEMP_ASC_DOWN='1' or (RESET ='0' and SENSOR_UP ='0' and SENSOR_IN = '0')) then
      SENSOR_DOWN <= '1';
      SENSOR_UP <= '0';
      SENSOR_IN <= '0';
    end if;
    if (FULL = '1') then
        FULL_EXIT <= '1';
    else
        FULL_EXIT <= '0';
    end if;
    case current_state is
        when S0 =>
            if ((BUTTON_UP = '1' and SENSOR_UP = '1' and FULL = '0') or 
            (BUTTON_DOWN = '1' and SENSOR_UP = '1')) then 
                SENSOR_UP <= '0';
                SENSOR_IN <= '1';
                SENSOR_DOWN <= '0';
                FLAG_TEMP_ASC_DOWN <= '0';
                next_state <= S1; --Bajo    
            elsif ((BUTTON_UP = '1' and SENSOR_DOWN = '1' and FULL = '0') or 
            (BUTTON_DOWN = '1' and SENSOR_DOWN = '1')) then 
                SENSOR_DOWN <= '0';
                SENSOR_IN <= '1';
                SENSOR_UP <= '0';
                FLAG_TEMP_ASC_UP <= '0';
                next_state <= S2; --Subo 
            end if;
        when S1 =>
            if SENSOR_DOWN = '1' then 
                next_state <= S0;
            end if;
        when S2 => 
            if SENSOR_UP = '1' then 
                next_state <= S0;
            end if;
        when others =>
            next_state <= S0;
    end case;
 end process;

 output_decod: process (current_state)
 begin
 LIGHT_UP <= (OTHERS => '0');
 LIGHT_DOWN <= (OTHERS => '0');
 case current_state is
    when S0 =>
      LIGHT_ESTADOS <= "100";
      if(SENSOR_UP= '1' or (RESET ='0' and SENSOR_DOWN ='0')) then
        LIGHT_UP <= "010";
        LIGHT_DOWN <= "100";
      elsif(SENSOR_DOWN= '1' or (RESET ='0' and SENSOR_UP ='0' and SENSOR_IN = '0')) then
        LIGHT_UP <= "100";
        LIGHT_DOWN <= "010";
      end if;
    when S1 =>
        LIGHT_ESTADOS <= "010";
        LIGHT_UP <= "110";
        LIGHT_DOWN <= "110";
    when S2 => 
        LIGHT_ESTADOS <= "001";
        LIGHT_UP <= "110";
        LIGHT_DOWN <= "110";
 end case;
 end process;
 
 LED_BUTTON_DOWN <= BUTTON_DOWN;
 LED_BUTTON_UP <= BUTTON_UP;
 
end behavioral;