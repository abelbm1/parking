library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.all;

entity contador_plazas is
generic (
    WIDTH : positive := 7 -- capacidad m�xima del parking
);
    Port ( PLAZAS : in STD_LOGIC_VECTOR (WIDTH -1 downto 0);
           LEDS   : out STD_LOGIC_VECTOR (WIDTH -1 downto 0);
           CLK    : in std_logic;
           RESET  : in std_logic;
           FULL   : out std_logic
           );
end contador_plazas;

architecture Behavioral of contador_plazas is

signal SALIDA_i : natural := 0;--se�al intermedia para la salida
signal FULL_i : std_logic := '0';

function hw_loop(v: std_logic_vector) return natural is
    variable h: natural;
    begin
        h := 0;
        for i in v'range loop
            if v(i) = '1' then
                h := h + 1;
            end if;
        end loop;
        return h;
end function hw_loop;

begin
--contador de plazas libres       
--proceso sincrono de asignacion a las se�ales intermedias
process (CLK, RESET)
begin
    if RESET = '0' then
    --a 0 todo cuando salte el reset  
        SALIDA_i <= 0;
        FULL_i <= '0';
    --flanco de subida del reloj
     elsif rising_edge(CLK) then
         --tratar el led de libre/ocupado
         SALIDA_i <= hw_loop(PLAZAS);
         if SALIDA_i = width then
              FULL_i <= '1';
         else
              FULL_i <= '0';
         end if;
      end if;
LEDS <= PLAZAS;
FULL <= FULL_i; 
end process; 
end Behavioral;