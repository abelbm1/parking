library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
USE ieee.std_logic_arith.ALL;
USE ieee.std_logic_unsigned.ALL;

entity Entrada_Parking is
generic (
    TOTAL : natural := 7 -- capacidad m�xima del parking
);
Port ( 
        CLK : in std_logic;
        RESET : in std_logic;
        BUTTON_ENTRY : in std_logic;
        BUTTON_EXIT : in std_logic;
        TEMP : in std_logic; --activa a TICKET siempre que haya hueco en el parking
        CONT : out natural; --aforo del parking
        TICKET : out std_logic;
        FULL : out std_logic
    );
end Entrada_Parking;

architecture Behavioral of Entrada_Parking is
signal TICKET_i : std_logic := '0';
signal CONT_i : NATURAL := 0;
signal FULL_i : STD_LOGIC := '0';

begin

    gestion_botones : process(CLK,RESET,CONT_i,BUTTON_ENTRY,BUTTON_EXIT, TEMP)
    begin
        if RESET = '0' then
        --a 0 todo cuando salte el reset  
            CONT_i <= 0;
            TICKET_i <= '0';
        --flanco de subida del reloj
        elsif rising_edge(CLK) then
            if CONT_i < TOTAL  and BUTTON_ENTRY = '1' then --hay plazas libres y se pulsa el bot�n de entrada
                CONT_i <= CONT_i + 1;
              --  TICKET_i <= '1';
           -- else
               --TICKET_i <= '0';
            end if;
            if BUTTON_ENTRY = '1' and  CONT_i < TOTAL  then --if TEMP = '1' and  CONT_i < TOTAL  then
                TICKET_i <= '1'; --enciende ticket
            elsif TEMP = '1' then --else  --temporizador llega a cero
                TICKET_i <= '0'; --apaga ticket
            end if;
            if ((BUTTON_EXIT = '1') and (CONT_i>0)) then
                CONT_i <= CONT_i - 1;
            end if;
        end if;
        TICKET <= TICKET_i;
       -- TICKET <= BUTTON_ENTRY;
        CONT <= CONT_i;
    end process;
    gestion_capacidad : process (CLK,RESET,CONT_i)
        begin
        if RESET = '0' then
        --a 0 todo cuando salte el reset  
            FULL_i <= '0';
        --flanco de subida del reloj
         elsif rising_edge(CLK) then
            if CONT_i = TOTAL  then
                FULL_i <= '1';
            else
                FULL_i <= '0';
            end if;
         end if;
        FULL <= FULL_i;
    end process;
end Behavioral;


