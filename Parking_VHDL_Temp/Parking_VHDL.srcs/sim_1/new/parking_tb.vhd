library ieee;
use ieee.std_logic_1164.all;

entity parking_tb is
end parking_tb;

architecture tb of parking_tb is

    component parking
        port (BUTTON_UP             : in std_logic;
              BUTTON_DOWN           : in std_logic;
              BUTTON_ENTRY          : in std_logic;
              BUTTON_EXIT           : in std_logic;
              BUTTON_TICKET_OFF     : in std_logic; 
              CLK                   : in std_logic;
              RESET                 : in std_logic;
              PLAZAS                : in std_logic_vector (6 downto 0);
              LEDS_UP               : out std_logic_vector (2 downto 0);
              LEDS_DOWN             : out std_logic_vector (2 downto 0);
              DISPLAY_PLAZAS        : out std_logic_vector (6 downto 0);
              FULL                  : out std_logic;
              FULL_DOWN             : out std_logic;
              INDICADOR_PLAZAS      : out std_logic_vector (6 downto 0);
              LED_TICKET            : out std_logic
        );
    end component;

    signal BUTTON_UP             : std_logic;
    signal BUTTON_DOWN           : std_logic;
    signal BUTTON_ENTRY          : std_logic;
    signal BUTTON_EXIT           : std_logic;
    signal BUTTON_TICKET_OFF     : std_logic;
    signal PLAZAS                : std_logic_vector (6 downto 0);
    signal CLK                   : std_logic;
    signal RESET                 : std_logic;
    signal LEDS_UP               : std_logic_vector (2 downto 0);
    signal LEDS_DOWN             : std_logic_vector (2 downto 0);
    signal DISPLAY_PLAZAS        : std_logic_vector (6 downto 0);
    signal FULL                  : std_logic;
    signal FULL_DOWN             : std_logic;
    signal INDICADOR_PLAZAS      : std_logic_vector (6 downto 0);
    signal LED_TICKET            : std_logic;

    constant TbPeriod : time := 10 ns; -- EDIT Put right period here
    signal TbClock : std_logic := '0';
    signal TbSimEnded : std_logic := '0';

begin

    dut : parking
    port map (BUTTON_UP             => BUTTON_UP,
              BUTTON_DOWN           => BUTTON_DOWN,
              BUTTON_ENTRY          => BUTTON_ENTRY,
              BUTTON_EXIT           => BUTTON_EXIT,
              BUTTON_TICKET_OFF     => BUTTON_TICKET_OFF,
              PLAZAS                => PLAZAS,
              CLK                   => CLK,
              RESET                 => RESET,
              LEDS_UP               => LEDS_UP,
              LEDS_DOWN             => LEDS_DOWN,
              DISPLAY_PLAZAS        => DISPLAY_PLAZAS,
              FULL                  => FULL,
              FULL_DOWN             => FULL_DOWN,
              INDICADOR_PLAZAS      => INDICADOR_PLAZAS,
              LED_TICKET            => LED_TICKET
    );

    -- Clock generation
    TbClock <= not TbClock after TbPeriod/2 when TbSimEnded /= '1' else '0';

    -- EDIT: Check that CLK is really your main clock signal
    CLK <= TbClock;

    stimuli : process
    begin
        -- EDIT Adapt initialization as needed
        RESET <= '1';
        BUTTON_UP <= '0';
        BUTTON_DOWN <= '0';
        BUTTON_ENTRY <= '0';
        BUTTON_EXIT <= '0';
        BUTTON_TICKET_OFF <= '0';
        PLAZAS <= (OTHERS => '0');

        -- Reset generation
        -- EDIT: Check that RESET is really your reset signal
       -- RESET <= '0', '1' after 10 ns;
        wait for 0.5 us;
       -- RESET <= '0', '1' after 10 ns;
        -- EDIT Add stimuli here
        --for i in 1 to 4 loop
        --BUTTON_ENTRY <= '1';
        --wait for 10 ns;
        --BUTTON_ENTRY <= '0';
       -- wait for 100 ns;
       -- end loop;

       -- BUTTON_EXIT <= '1';
       -- wait for 10 ns;
        --BUTTON_EXIT <= '0';
        
        BUTTON_ENTRY <= '1';
        wait for 0.5 us;
        BUTTON_ENTRY <= '0';
        wait for 1 us;
        BUTTON_TICKET_OFF <= '1';
        wait for 0.5 us;
        BUTTON_TICKET_OFF <= '0';
        for i in 1 to 3 loop
        wait for 1 us;
        BUTTON_UP <= '1';
        wait for 0.5 us;
        BUTTON_UP <= '0';
        wait for 4 us;
        BUTTON_DOWN <= '1';
        wait for 0.5 us;
        BUTTON_DOWN <= '0';
        wait for 4 us;
        BUTTON_UP <= '1';
        wait for 0.25 us;
        BUTTON_UP <= '0';
        wait for 4 us;
        BUTTON_UP <= '1';
        wait for 0.25 us;
        BUTTON_UP <= '0';
        end loop;
       -- BUTTON_ENTRY <= '1';
        --wait for 0.25 us;
        --BUTTON_ENTRY <= '0';
        --wait for 0.75 us;
        --BUTTON_TICKET_OFF <= '1';
       -- wait for 0.25 us;
        --BUTTON_TICKET_OFF <= '0';
       -- wait for 300 ns;
       -- PLAZAS <= "111111010001";
       -- wait for 300 ns;
        --PLAZAS <= "111111111111";
        --wait for 300 ns;
        
       -- BUTTON_ENTRY <= '1';
      --  wait for 10 ns;
       -- BUTTON_ENTRY <= '0';
       
      --  BUTTON_DOWN <= '1';
       -- wait for 10 ns;
        --BUTTON_DOWN <= '0';
        
       -- wait for 300 ns;
        
      --  BUTTON_UP <= '1';
       -- wait for 10 ns;
       -- BUTTON_UP <= '0';
       -- wait for 300 ns;
       -- BUTTON_UP <= '1';
       -- wait for 10 ns;
       -- BUTTON_UP <= '0';
       -- wait for 300 ns;
       -- BUTTON_UP <= '1';
       -- wait for 10 ns;
        --BUTTON_UP <= '0';
        wait for 200 us;

        -- Stop the clock and hence terminate the simulation
        TbSimEnded <= '1';
        wait;
    end process;

end tb;

