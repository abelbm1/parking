library ieee;
use ieee.std_logic_1164.all;

entity clk1Hz_tb is
end clk1Hz_tb;

architecture tb of clk1Hz_tb is

    component clk1Hz
        port (CLK    : in std_logic;
              CLK_1hz   : out std_logic);
    end component;

    signal CLK   : std_logic := '0';
    signal CLK_1hz : std_logic;

    constant TbPeriod : time := 10 ns; -- EDIT Put right period here
    signal TbClock : std_logic := '0';
    signal TbSimEnded : std_logic := '0';

begin

    dut : clk1Hz
    port map (CLK    => CLK,           
              CLK_1hz   => CLK_1hz);

    -- Clock generation
    TbClock <= not TbClock after TbPeriod/2 when TbSimEnded /= '1' else '0';

    -- EDIT: Check that CLK_IN  is really your main clock signal
    CLK <= TbClock;
    stimuli : process
    begin
        -- EDIT Adapt initialization as needed

        -- Reset generation
        -- EDIT: Check that rst is really your reset signal

        -- EDIT Add stimuli here
        wait for 100 * TbPeriod;

        -- Stop the clock and hence terminate the simulation
        TbSimEnded <= '1';
        wait;
    end process;

end tb;
